package com.softserve.edu.storage.dao;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class RAMStorage<T> implements Repository<T> {

    private Map<Long, T> map = new HashMap<>();
    private long key;

    @Override
    public long save(T entity) {
        map.put(++key, entity);
        return key;
    }

    @Override
    public T findOne(long primaryKey) {
        return map.get(primaryKey);
    }

    @Override
    public Collection<T> findAll() {
        return map.values();
    }

    @Override
    public long count() {
        return map.size();
    }

    @Override
    public T delete(long primaryKey) {
        return map.remove(primaryKey);
    }

    @Override
    public boolean exists(long primaryKey) {
        return map.containsKey(primaryKey);
    }

    public void clear() {
        map.clear();
    }
}
