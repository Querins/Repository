package com.softserve.edu.storage.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

public interface Repository<T> {

    /**
     *
     * @param entity object to save
     * @return primary key for saved entity
     */
    long save(T entity);

    /**
     *
     * @param primaryKey key which unambiguously identifies entity in database
     * @return entity found
     */
    T findOne(long primaryKey);

    Collection<T> findAll();

    /**
     *
     * @return Number of entities, stored in repository
     */
    long count();

    T delete(long primaryKey);

    /**
     * Checks if given entity exists
     * @param primaryKey
     * @return true if this entity exists in repository
     */
    boolean exists(long primaryKey);

    void clear();

}