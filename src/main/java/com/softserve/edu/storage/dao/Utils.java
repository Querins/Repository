package com.softserve.edu.storage.dao;

import com.softserve.edu.storage.dao.mysql.annotations.Column;
import com.softserve.edu.storage.dao.mysql.annotations.Entity;
import com.softserve.edu.storage.dao.mysql.annotations.Id;
import com.softserve.edu.storage.dao.mysql.annotations.Table;
import com.sun.istack.internal.NotNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.text.MessageFormat;
import java.util.*;

public class Utils<T> {

    static {
        Map<Class, String> m = new HashMap<>();
        m.put(int.class, "INT");
        m.put(String.class, "TEXT");
        m.put(double.class, "DOUBLE");
        m.put(char.class, "CHAR");
        m.put(long.class, "BIGINT");
        m.put(boolean.class, "TINYINT");
        m.put(short.class, "SMALLINT");
        m.put(byte.class, "TINYINT");
        types = Collections.unmodifiableMap(m);
    }

    final private static Map<Class, String> types; // maps java classes on SQL types

    private Connection c;
    private final Class<T> type;
    private final String primaryKeyName;
    private final String tableName;
    private final Map<String, String> fieldNameToColumn;
    private final Constructor<T> constructor;

    public Utils(@NotNull Connection c, Class<T> type) {
        this.c = c;
        this.type = type;
        primaryKeyName = getPrimaryKeyName();
        tableName = getTableName();
        fieldNameToColumn = Collections.unmodifiableMap(fillFieldToColumnMap());
        constructor = getDefaultConstructor();
        validateClass();
    }

    /**
     *
     * @throws SQLException
     */
    public void createTable() throws SQLException, NoSuchFieldException {

        String name = getTableName();

        StringBuilder sb = new StringBuilder();
        sb.append(String.format("CREATE TABLE %s (", name));

//        Field[] fields = type.getDeclaredFields();
//
//        for(int i = 0; i < fields.length; i++) {
//            Field f = fields[i];
//            f.setAccessible(true);
//            if(f.isAnnotationPresent(Column.class)) {
//                String columnName = fieldNameToColumn.get(f.getName());
//                String sqlType = types.get(f.getType());
//                if(sqlType == null) {
//                    throw new RuntimeException("Cannot map " + f.getType().getName() + " on SQL type");
//                }
//                sb.append(String.format("%s %s", columnName, sqlType));
//                if(i < fields.length - 1) {
//                    sb.append(" ,");
//                }
//            }
//        }
        int size = fieldNameToColumn.size();
        Iterator<String> iterator = fieldNameToColumn.keySet().iterator();

        for(int i = 0; i < size; i++) {
            String fieldName = iterator.next();
            String columnName = fieldNameToColumn.get(fieldName);
            String sqlType = types.get(type.getDeclaredField(fieldName).getType());
            if (sqlType == null) {
                throw new RuntimeException("Cannot map " + " on SQL type");
            }
            if(fieldName.equals(primaryKeyName)) {
                sb.append(String.format("%s %s NOT NULL AUTO_INCREMENT, ", columnName, sqlType));
            } else {
                sb.append(String.format("%s %s, ", columnName, sqlType));
            }
        }

        sb.append(String.format(" PRIMARY KEY(%s)", primaryKeyName));

        sb.append(");");
        String sqlQuery = sb.toString();
        c.createStatement().execute(sqlQuery);
    }

    private void validateClass() {

        if(type.isInterface() || type.isEnum() || !type.isAnnotationPresent(Entity.class)) {
            throw new IllegalArgumentException("Entity class must be declared as class and marked with @Entity annotation");
        }
    }

    private Constructor<T> getDefaultConstructor() {
        try {
            Constructor<T> c = type.getDeclaredConstructor();
            c.setAccessible(true);
            return c;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Each entity must provide default constructor");
        }
    }

    private Map<String, String> fillFieldToColumnMap() {
        Map<String, String> m = new HashMap<>();
        for(Field f : type.getDeclaredFields()) {
            if(f.isAnnotationPresent(Column.class)) {
                String columnName = f.getAnnotation(Column.class).value();
                if(columnName.equals("")) {
                    columnName = f.getName();
                }
                m.put(f.getName(), columnName);
            }
        }
        m.put(primaryKeyName, primaryKeyName);
        return m;
    }

    private String getPrimaryKeyName() {

        for(Field f : type.getDeclaredFields()) {
            Id id = f.getAnnotation(Id.class);
            if(id != null) {
                if(!f.getType().equals(long.class)) {
                    throw new RuntimeException("Primary key must have type long");
                }
                return f.getName();
            }
        }
        throw new RuntimeException("Every entity must have a primary key");
    }

    private String getTableName() {

        if(type.isAnnotationPresent(Table.class)) {
            return type.getAnnotation(Table.class).value();
        } else {
            return type.getSimpleName();
        }

    }

    private boolean validateExistingTable(Class<T> entity) {
        return true;
    }

    private String toSQLLiteral(Object o) {

        if(o == null) {
            return "null";
        }

        Class clazz = o.getClass();

        if(o instanceof Number || o instanceof Character) return o.toString();
        if(o instanceof Boolean) return (boolean)o ? "'1'" : "'0";
        if(o instanceof String) {
            return String.format("'%s'", o.toString());
        }
        return o.toString();
    }

    public long addToTable(T entity) throws SQLException, ReflectiveOperationException {

        long key = -1L;

        if(!tableExists()) {
            createTable();
        }

        StringBuilder columnNames = new StringBuilder();
        StringBuilder values = new StringBuilder();

        for(String fieldName : fieldNameToColumn.keySet()) {

            if(fieldName.equals(primaryKeyName)) {
                continue;
            }
            columnNames.append(fieldNameToColumn.get(fieldName)).append(", ");
            Field f = type.getDeclaredField(fieldName);
            f.setAccessible(true);
            Object fieldValue = f.get(entity);
            values.append(toSQLLiteral(fieldValue)).append(", ");

        }

//        columnNames.substring(0, columnNames.length() - 2);
//        values.substring(0, values.length() - 2);

        String sqlQuery = String.format("INSERT INTO %s (%s) VALUES (%s);", tableName,
                columnNames.substring(0, columnNames.length() - 2),
                values.substring(0, values.length() - 2));

        Statement st = c.createStatement();
        st.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);

        ResultSet generatedKeys = st.getGeneratedKeys();
        if (generatedKeys != null && generatedKeys.next()) {
            key = generatedKeys.getLong(1);
        }

        type.getDeclaredField(primaryKeyName).set(entity, key);

        return key;
    }

    private List<T> parseResultSet(ResultSet rs) throws SQLException, ReflectiveOperationException {

        LinkedList<T> entities = new LinkedList<>();
        while(rs.next()) {
            T currentEntity = constructor.newInstance();
            for(String fieldName : fieldNameToColumn.keySet()) {
                Field f = type.getDeclaredField(fieldName);
                Object retrievedValue = rs.getObject(fieldNameToColumn.get(fieldName), f.getType());
                f.setAccessible(true);
                f.set(currentEntity, retrievedValue);
                f.setAccessible(false);
            }
            entities.add(currentEntity);
        }
        rs.close();
        return entities;
    }

    public T retrieve(long primaryKey) throws SQLException, ReflectiveOperationException {

        final String sqlQuery = String.format("SELECT * FROM %s WHERE %s = %d", tableName, primaryKeyName, primaryKey);

        try(ResultSet rs = c.createStatement().executeQuery(sqlQuery)) {

            List<T> list = parseResultSet(rs);

            if (list.size() < 1) {
                return null;
            } else {
                return list.get(0);
            }
        }

    }

    public List<T> retrieveAll() throws SQLException, ReflectiveOperationException {
        String sqlQuery = String.format("SELECT * FROM %s", tableName);
        try(ResultSet rs = c.createStatement().executeQuery(sqlQuery)) {
            return parseResultSet(rs);
        }

    }

    public void delete(long pk) throws SQLException {
        String sqlQuery = String.format("DELETE FROM %s WHERE %s=%s", tableName.toLowerCase(), primaryKeyName, pk);
        try(Statement st = c.createStatement()) {
            st.execute(sqlQuery);
        }
    }

    public void clearAll() throws SQLException {
        String sqlQuery = String.format("DELETE FROM %s", tableName.toLowerCase());
        c.createStatement().execute(sqlQuery);
    }

    public long count() throws SQLException, ReflectiveOperationException {
        String sqlQuery = String.format("SELECT COUNT(*) FROM %s", tableName);
        try(ResultSet rs = c.createStatement().executeQuery(sqlQuery)) {
            if(rs.next()) {
                return rs.getLong(1);
            } else {
                throw new RuntimeException("Empty resultSet");
            }
        }
    }

    public boolean tableExists() throws SQLException {
        try(ResultSet rs = c.getMetaData().getTables(null, null, this.tableName, new String[]{"TABLE"})) {
            while (rs.next()) {
                if (rs.getString("TABLE_NAME").equalsIgnoreCase(this.tableName)) {
                    return true;
                }
            }
            return false;
        }
    }
}
