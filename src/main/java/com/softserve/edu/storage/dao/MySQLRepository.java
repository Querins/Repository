package com.softserve.edu.storage.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MySQLRepository<T> implements Repository<T> {

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Cannot find MySQL jdbc driver in the classpath");
        }
    }

    private Connection c;
    private Utils<T> u;

    private MySQLRepository(Class<T> type) {
        u = new Utils<>(c, type);
        try {
            if (!u.tableExists()) {
                u.createTable();
            }
        } catch (SQLException e) {
            throw new RuntimeException("SQL exception ", e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Internal exception: No such field ", e);
        }
    }

    public MySQLRepository(String schema, String user, String password, Class<T> type) {
        try {
            c = DriverManager.getConnection("jdbc:mysql://localhost/" + schema, user, password);
            u = new Utils<>(c, type);
            if (!u.tableExists()) {
                u.createTable();
            }
        } catch (SQLException e) {
            throw new RuntimeException("Cannot connect to database", e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Internal exception: No such field ", e);
        }

    }

    public MySQLRepository(Connection c, Class<T> type){
        this.c = c;
        u = new Utils<>(c, type);
        try {
            if (!u.tableExists()) {
                u.createTable();
            }
        } catch (SQLException e) {
            throw new RuntimeException("SQL Exception", e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException("Internal exception: No such field ", e);
        }
    }

    @Override
    public long save(T entity) {
        try {
            return u.addToTable(entity);
        } catch (SQLException e) {
            throw new RuntimeException("SQL exception: ", e);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Internal parsing exception ", e);
        }
    }

    @Override
    public T findOne(long primaryKey) {
        try {
            return u.retrieve(primaryKey);
        } catch (SQLException e) {
            throw new RuntimeException("SQL Exception occurred", e);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Exception occurred during using reflection", e);
        }
    }

    @Override
    public List<T> findAll() {
        try {
            return u.retrieveAll();
        } catch (SQLException e) {
            throw new RuntimeException("SQL exception: ", e);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Internal parsing exception ", e);
        }
    }

    @Override
    public long count() {
        try {
            return u.count();
        } catch (SQLException e) {
            throw new RuntimeException("SQL Exception: ", e);
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Internal parsing exception ", e);
        }
    }

    @Override
    public T delete(long primaryKey) {

        try {
            T o = findOne(primaryKey);
            u.delete(primaryKey);
            return o;
        } catch (SQLException e) {
            throw new RuntimeException("SQL Exception: ", e);
        }
    }

    @Override
    public boolean exists(long primaryKey) {
        return findOne(primaryKey) != null;
    }

    @Override
    public void clear() {
        try {
            u.clearAll();
        } catch (SQLException e) {
            throw new RuntimeException("SQL Exception: ", e);
        }
    }
}
