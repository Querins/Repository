package com.softserve.edu.storage.dao;

import com.softserve.edu.storage.dao.mysql.annotations.Column;
import com.softserve.edu.storage.dao.mysql.annotations.Entity;
import com.softserve.edu.storage.dao.mysql.annotations.Id;

@Entity
class Car {

    @Column
    private String name;
    @Column
    private int maxSpeed;
    @Id
    long id;

    private Car() {}

    public Car(String name, int maxSpeed) {
        this.name = name;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (maxSpeed != car.maxSpeed) return false;
        return name.equals(car.name);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + maxSpeed;
        return result;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}