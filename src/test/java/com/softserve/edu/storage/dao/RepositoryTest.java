package com.softserve.edu.storage.dao;

import com.softserve.edu.storage.dao.mysql.annotations.Column;
import com.softserve.edu.storage.dao.mysql.annotations.Entity;
import com.softserve.edu.storage.dao.mysql.annotations.Id;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(value = Parameterized.class)
public class RepositoryTest {

    private Repository<Car> repo;

    public RepositoryTest(Repository<Car> repo) {
        this.repo = repo;
    }

    @Parameterized.Parameters
    public static Collection<Repository<Car>[]> data() {
        Repository<Car> a = new MySQLRepository<>("repository", "root", "toor", Car.class);
        Repository<Car> b = new RAMStorage<>();
        List<Repository<Car>[]> l = new ArrayList<>(2);
        l.add(new Repository[] {a});
        l.add(new Repository[] {b});
        return l;
    }

    @Before
    public void clearRepo() {
        repo.clear();
    }

    @Test
    public void saveFindTestTest() {
        Car c = new Car("Lamborghini", 385);
        long pk = repo.save(c);

        Car car = repo.findOne(pk);
        assertTrue(c.equals(car));
        Car cNull = repo.findOne(-9);
    }

    @Test
    public void clearTest() {
        List<Car> cars = new ArrayList<>(100);
        for(int i = 0; i < 100; i++) {
            cars.add(new Car(String.valueOf(i), i));
            repo.save(cars.get(i));
        }
        repo.clear();
        assertEquals(0, repo.count() );
    }

    @Test
    public void getAllTest() {
        List<Car> cars = new ArrayList<>(100);
        for(int i = 0; i < 100; i++) {
            cars.add(new Car(String.valueOf(i), i));
            repo.save(cars.get(i));
        }
        Collection<Car> c = repo.findAll();
        assertTrue(c.containsAll(cars));
    }

    @Test
    public void exists() {
        long pk = repo.save(new Car("random", 100));
        assertTrue(repo.exists(pk));

        assertFalse(repo.exists(-6));
    }

    @Test
    public void deleteTest() {
        long pk = repo.save(new Car("random", 100));
        repo.delete(pk);
        assertFalse(repo.exists(pk));
    }

}
